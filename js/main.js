function Tree(container) {
  const privateApi = {
    isRecursiveRender: 1,
    nodes: [
      {
        label: 'Root',
        childs: [
          {
            label: 'Leaf 1',
            childs: [],
          },
          {
            label: 'Leaf 2',
            childs: [
              {
                label: 'Leaf 3',
                childs: [],
              },
            ],
          },
        ],
      },
    ],
    container,
    toggleMode: () => {
      privateApi.isRecursiveRender = !privateApi.isRecursiveRender;
      privateApi.render();
      return privateApi.isRecursiveRender;
    },
    handleClick: (item) => {
      const label = prompt('Enter category label');
      if (label) {
        privateApi.addNode(item, label);
      }
    },
    addNode: (parentCategory, label) => {
      parentCategory.childs.unshift({
        label,
        childs: [],
      });
      privateApi.render();
    },
    renderNode: (item, container) => {
      const li = document.createElement('li');
      const label = document.createTextNode(item.label);
      const span = document.createElement('span');
      span.style.pointerEvents = 'none';
      span.appendChild(label);
      li.appendChild(span);
      const ul = document.createElement('ul');
      li.append(ul);
      container.querySelector('ul').appendChild(li);

      li.addEventListener('click', (e) => {
        e.stopPropagation();
        privateApi.handleClick(item);
      });
      return li;
    },
    renderRecursively: (category, container) => {
      if (!container) {
        privateApi.container.appendChild(document.createElement('ul'));
        container = privateApi.container;
      }
      category.map((subcategory) => {
        const item = privateApi.renderNode(subcategory, container);
        if ('childs' in subcategory && subcategory.childs.length) {
          privateApi.renderRecursively(subcategory.childs, item);
        }
      });
    },
    renderIteratively: () => {
      privateApi.container.appendChild(document.createElement('ul'));
      let container = privateApi.container;
      const queue = [
        {
          item: privateApi.nodes[0],
          container,
        },
      ];
      let category;
      while (queue.length) {
        category = queue.shift();
        container = privateApi.renderNode(category.item, category.container);
        for (let i = 0; i < category.item.childs.length; i += 1) {
          queue.push({ item: category.item.childs[i], container });
        }
      }
    },
    render: () => {
      while (privateApi.container.firstChild) {
        privateApi.container.removeChild(privateApi.container.firstChild);
      }
      const modeLabel = document.querySelector('.mode');
      if (privateApi.isRecursiveRender) {
        modeLabel.textContent = 'Recursive';
        privateApi.renderRecursively(privateApi.nodes);
      } else {
        modeLabel.textContent = 'Iterative';
        privateApi.renderIteratively();
      }
    },
  };
  this.toggleMode = privateApi.toggleMode;
  privateApi.render();
}

const tree = new Tree(document.querySelector('.tree'));
const button = document.querySelector('button');

button.addEventListener('click', () => {
  tree.toggleMode();
});
